import { urlRoot } from 'src/services/api/common/const'

export const getAvatarPath = (pathAvatar: string) => (
  pathAvatar ? `${urlRoot}/resources${pathAvatar}` : '')
