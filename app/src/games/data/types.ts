export type GameCard = {
  id: string,
  name: string,
  title: string
}
