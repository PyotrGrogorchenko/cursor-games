
#!/bin/sh -e

psql --variable=ON_ERROR_STOP=1 --username admin <<-EOSQL
    CREATE ROLE admin WITH LOGIN PASSWORD ${POSTGRES_PASSWORD};
    CREATE DATABASE games OWNER = admin;
    GRANT ALL PRIVILEGES ON DATABASE games TO admin;
EOSQL