import React, {
  FC, useEffect, useState,
  useCallback
} from 'react'
import { Keys } from '@games/aux/types'
import { userDataSelector } from '@store/selectors'
import { getScore, saveScore } from '@games/aux/api/saveScore'
import { Props } from './types'
import { View } from '../View'
import {
  getModelData, onController, clear, observer, updateScoreBest
} from '../model'

const Controller: FC<Props> = () => {
  const [modelData, setModelData] = useState(getModelData())
  const { condition } = modelData
  const userData = userDataSelector()

  const onKeyDown = useCallback(({ key }: KeyboardEvent) => {
    onController(key as Keys)
  }, [])

  const onModel = useCallback(() => {
    setModelData(getModelData())
  }, [])

  useEffect(() => {
    window.addEventListener('keydown', onKeyDown)
    observer.subscribe(onModel)
    getScore(Number(userData?.id), modelData)
      .then(res => {updateScoreBest(res)})

    return () => {
      window.removeEventListener('keydown', onKeyDown)
      observer.unsubscribe(onModel)
      clear()
    }
  }, [])

  useEffect(() => {
    if (!userData || modelData.condition !== 'gameOver') return
    saveScore(userData.id, modelData)
  }, [condition])

  return <View model={modelData}/>
}

export const ControllerTSX = Controller
