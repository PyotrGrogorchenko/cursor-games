import { exe } from './methods'
import { DataLogin } from './types'

export const login = async (data: DataLogin) => {
  await exe('postUser', data)
}
