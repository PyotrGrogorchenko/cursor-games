import { exe } from '@apiDb/score/methods'
import { ResGetScore } from '@apiDb/score/types'
import { BaseData } from '../types'

export const saveScore = (userId: number, data: BaseData) => {
  if (data.score <= data.scoreBest) return
  exe('postScore', {
    userId,
    score: data.score,
    gameId: data.gameId
  })
}

export const getScore = async (userId: number, data: BaseData) => {
  const res = await exe('getScore', {
    userId,
    gameId: data.gameId
  }) as ResGetScore
  if (res.status === 200) return res.data.score
  return 0
}
