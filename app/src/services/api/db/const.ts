export const urlRoot = `${document.location.protocol}//${document.location.hostname}:8000/api`

export const url = {
  score: `${urlRoot}/score`,
  user: `${urlRoot}/users`
}
