# cursor-games

Mini-games for the keyboard cursor

## Install

> node 14.x

```bash
$ npm install
// development
$ npm run dev

```
## Credits

* [PyotrGrogorchenko](https://github.com/PyotrGrogorchenko) - Developer