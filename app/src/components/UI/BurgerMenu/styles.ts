import { createStyles } from '@material-ui/core/styles'

export const styles = () => createStyles({
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  }
})
