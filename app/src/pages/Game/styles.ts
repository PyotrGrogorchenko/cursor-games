import { createStyles } from '@material-ui/core/styles'

export const styles = () => createStyles({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '80vh'
  }
})
