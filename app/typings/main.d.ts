type LooseObject = {
  [key: string]: any
}
type Ctx = CanvasRenderingContext2D
