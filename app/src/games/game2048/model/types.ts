import { BaseData } from '@games/aux/types'

export type Data = {
  items: number[]
  values: LooseObject
  moveInterval: number
} & BaseData

const height = 4
const width = 4

export const initialModel: Data = {
  gameId: '2',
  score: 0,
  scoreBest: 0,
  condition: 'begin',
  height,
  width,
  items: [],
  values: {},
  moveInterval: 20
}
