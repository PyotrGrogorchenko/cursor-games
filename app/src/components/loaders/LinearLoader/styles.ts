import { createStyles } from '@material-ui/core/styles'

export const styles = () => createStyles({
  root: {
    zIndex: 1100,
    position: 'fixed',
    width: '100%'
  }
})
